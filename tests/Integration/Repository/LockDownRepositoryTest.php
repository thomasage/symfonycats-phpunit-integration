<?php
declare(strict_types=1);

namespace App\Tests\Integration\Repository;

use App\Enum\LockDownStatus;
use App\Factory\LockDownFactory;
use App\Repository\LockDownRepository;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

final class LockDownRepositoryTest extends KernelTestCase
{
    use Factories, ResetDatabase;

    public function testIsInLockDownWithNoLockDownRows(): void
    {
        self::bootKernel();

        self::assertFalse($this->getLockDownRepository()->isInLockDown());
    }

    private function getLockDownRepository(): LockDownRepository
    {
        return self::getContainer()->get(LockDownRepository::class);
    }

    public function testIsInLockDownReturnsTrueIfMostRecentLockDownIsActive(): void
    {
        self::bootKernel();

        LockDownFactory::createOne([
            'createdAt' => new \DateTimeImmutable('-1 day'),
            'status' => LockDownStatus::ACTIVE,
        ]);
        LockDownFactory::createMany(5, [
            'createdAt' => new \DateTimeImmutable('-2 days'),
            'status' => LockDownStatus::ENDED,
        ]);

        self::assertTrue($this->getLockDownRepository()->isInLockDown());
    }

    public function testIsInLockDownReturnsFalseIfMostRecentIsNotActive(): void
    {
        self::bootKernel();

        LockDownFactory::createOne([
            'createdAt' => new \DateTimeImmutable('-1 day'),
            'status' => LockDownStatus::ENDED,
        ]);
        LockDownFactory::createMany(5, [
            'createdAt' => new \DateTimeImmutable('-2 days'),
            'status' => LockDownStatus::ACTIVE,
        ]);

        self::assertFalse($this->getLockDownRepository()->isInLockDown());
    }
}
