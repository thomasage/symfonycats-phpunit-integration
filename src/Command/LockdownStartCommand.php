<?php
declare(strict_types=1);

namespace App\Command;

use App\Service\LockDownHelper;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(name: 'app:lockdown:start')]
final class LockdownStartCommand extends Command
{
    public function __construct(private readonly LockDownHelper $lockDownHelper)
    {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $this->lockDownHelper->dinoEscaped();

        $io->caution('Lockdown started!!!!!');

        return Command::SUCCESS;
    }
}
