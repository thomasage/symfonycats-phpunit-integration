<?php
declare(strict_types=1);

namespace App\Service;

use App\Entity\LockDown;
use App\Enum\LockDownStatus;
use App\Message\LockDownStartedNotification;
use App\Repository\LockDownRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\MessageBusInterface;

final class LockDownHelper
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly GithubService $githubService,
        private readonly LockDownRepository $lockDownRepository,
        private readonly MessageBusInterface $messageBus,
    ) {
    }

    public function endCurrentLockDown(): void
    {
        $lockDown = $this->lockDownRepository->findMostRecent();
        if (!$lockDown instanceof LockDown) {
            throw new \LogicException('There is no lock down to end.');
        }

        $lockDown->setStatus(LockDownStatus::ENDED);
        $this->entityManager->flush();

        $this->githubService->clearLockDownAlerts();
    }

    public function dinoEscaped(): void
    {
        $lockDown = (new LockDown())
            ->setStatus(LockDownStatus::ACTIVE)
            ->setReason('Dino escaped... NOT good...');

        $this->entityManager->persist($lockDown);
        $this->entityManager->flush();

        $this->messageBus->dispatch(new LockDownStartedNotification());
    }
}
